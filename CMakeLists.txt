cmake_minimum_required (VERSION 2.6)
project (Bantu)

# Setting directories
set (CMAKE_BINARY_DIR ${CMAKE_SOURCE_DIR}/build)
set (EXECUTABLE_OUTPUT_PATH ${CMAKE_BINARY_DIR})
set (LIBRARY_OUTPUT_PATH ${CMAKE_BINRARY_DIR})

set (PROJECT_INCLUDE_DIR ${PROJECT_SOURCE_DIR}/include)

# Importing Include Directories
include_directories ("${PROJECT_INCLUDE_DIR}")
include_directories ("${PROJECT_SOURCE_DIR}")

# Add projects
add_subdirectory(Framework)
add_subdirectory(FrameworkTester)
