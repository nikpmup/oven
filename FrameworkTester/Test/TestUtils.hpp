#ifndef TESTUTILS_HPP
#define TESTUTILS_HPP

#include <iostream>
#include <string>

#define HEADER_LINE "=========================================================="

inline void printResult(bool result, const std::string &strTest) {
   const std::string strResult = result ? "Success: " : "Fail : ";

   std::cout << strResult << strTest << std::endl;
}

inline void printTestHeader(const std::string &strHeader) {
   std::cout << HEADER_LINE << std::endl << strHeader << std::endl
             << HEADER_LINE << std::endl;
}

#endif // TESTUTILS_HPP
