// Test
#include <Test/BagTester.hpp>
#include <Test/ComponentTypeTester.hpp>
#include <Test/ComponentTester.hpp>
#include <Test/EntityTester.hpp>

//----------------------------------------------------------------------------
//
int main() {
   TestBag();
   TestComponentType();
   TestComponents();
   TestEntityManager();
   //TestEntity();

   return 0;
}
