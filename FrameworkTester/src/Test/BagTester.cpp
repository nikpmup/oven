// Oven Libraries
#include <Oven/Utils/Debug.hpp>
#include <Oven/Utils/Bag.hpp>
// Test
#include <Test/BagTester.hpp>
#include <Test/TestUtils.hpp>

const static int kData[] = {
   1,
   2,
   3,
   4,
   5
};

const static int kDataSize(5);

// Adds several primitives
bool BagTestAdd(Bag<int> &bagInt);

// Checks if the bag is valid
bool BagTestContains(Bag<int> &bagInt);

// Test Remove/Empty
bool BagTestRemoveIdx(Bag<int> &bagInt);
bool BagTestRemoveVal(Bag<int> &bagInt);
bool BagTestRemoveLast(Bag<int> &bagInt);
bool BagTestRemoveBag(Bag<int> &bagInt);
bool BagTestRemoveSingleIdx(Bag<int> &bagInt, unsigned int idx);
bool BagTestRemoveSingleValue(Bag<int> &bagInt, unsigned int idx);
bool BagTestSetSingleIdx(Bag<int> &bagInt, unsigned int idx);

//----------------------------------------------------------------------------
//
void TestBag() {
   printTestHeader("Test Bag");
   D_COUT("Running testBag\n");
   bool result;
   Bag<int> intBag;

   // Testing Adding Values
   D_CMD(printTestHeader("Adding and Checking Values"));
   BagTestAdd(intBag);
   result = BagTestContains(intBag);
   printResult(result, "Add and check values");

   // Testing Remove Single Value by idx
   D_CMD(printTestHeader("Removing Single Value by Idx"));
   result = BagTestRemoveSingleIdx(intBag, 0);
   printResult(result, "Remove single by idx");

   // Testing Set Single Value by idx
   D_CMD(printTestHeader("Setting Single Value by Idx"));
   result = BagTestSetSingleIdx(intBag, 0);
   printResult(result, "Set single by idx");

   // Testing Remove Values by val
   D_CMD(printTestHeader("Remove All by Value"));
   result = BagTestRemoveVal(intBag);
   printResult(result, "Remove all by val");

   // Testing Remove Values by last
   D_CMD(printTestHeader("Remove All by Last Item"));
   BagTestAdd(intBag);
   result = BagTestRemoveVal(intBag);
   printResult(result, "Remove all by last");

   // Testing Remove Values by bag
   D_CMD(printTestHeader("Remove All by Bag"));
   BagTestAdd(intBag);
   result = BagTestRemoveVal(intBag);
   printResult(result, "Remove all by bag");

   // Testing Remove Values by idx
   D_CMD(printTestHeader("Remove All by Idx"));
   BagTestAdd(intBag);
   result = BagTestRemoveIdx(intBag);
   printResult(result, "Remove all by idx");


   // Testing Adding Bag
   {
      D_CMD(printTestHeader("Adding by bag"));
      Bag<int> tempBag;
      BagTestAdd(tempBag);
      result = BagTestRemoveIdx(intBag);
      printResult(result, "Add by bag");
   }
}

//----------------------------------------------------------------------------
//
bool BagTestAdd(Bag<int> &bagInt) {
   D_COUT("Running BagTestAdd\n");

   // Adding all values
   for (int value : kData) {
      bagInt.add(value);
   }

   return true;
}

//----------------------------------------------------------------------------
//
bool BagTestContains(Bag<int> &bagInt) {
   D_COUT("Running BagTestContains\n");
   bool valid = true;

   // Check if bag contains all data
   for (int value : kData) {
      value &= bagInt.contains(value);
   }

   return valid;
}

//----------------------------------------------------------------------------
//
bool BagTestRemoveIdx(Bag<int> &bagInt) {
   D_COUT("Running BagTestRemoveIdx\n");

   // Removing by idx
   for (unsigned int idx(0); bagInt.getCount(); idx++) {
      bagInt.removeIdx(idx);
   }
   // Check if all values removed
   return bagInt.isEmpty();
}

//----------------------------------------------------------------------------
//
bool BagTestRemoveVal(Bag<int> &bagInt) {
   D_COUT("Running BagTestRemoveVal\n");

   // Removing by value
   for (int value : kData) {
      bagInt.remove(value);
   }
   // Check if all values removed
   return bagInt.isEmpty();
}

//----------------------------------------------------------------------------
//
bool BagTestRemoveLast(Bag<int> &bagInt) {
   D_COUT("Running BagTestRemoveLast\n");

   // Removing by idx
   for (unsigned int idx(0); bagInt.getCount(); idx++) {
      bagInt.removeLast();
   }
   // Check if all values removed
   return bagInt.isEmpty();
}

//----------------------------------------------------------------------------
//
bool BagTestRemoveBag(Bag<int> &bagInt) {
   D_COUT("Running BagTestRemoveBag\n");
   Bag<int> tempBag;

   // Removing by value
   bagInt.removeAll(tempBag);

   // Check if all values removed
   return bagInt.isEmpty();
}

//----------------------------------------------------------------------------
//
bool BagTestRemoveSingleIdx(Bag<int> &bagInt, unsigned int idx) {
   D_COUT("Running BagTestRemoveSingleIdx\n");
   int removeVal(bagInt.removeIdx(idx));

   return removeVal == kData[idx];
}

//----------------------------------------------------------------------------
//
bool BagTestSetSingleIdx(Bag<int> &bagInt, unsigned int idx) {
   D_COUT("Running BagTestSetSingleIdx\n");
   bagInt.set(idx, kData[idx]);
   int bagCnt(bagInt.getCount());

   D_COUT2("Bag count = " << bagCnt << std::endl);
   return bagCnt == kDataSize - 1;
}
