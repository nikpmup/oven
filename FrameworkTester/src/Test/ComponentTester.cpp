// Test
#include <Test/ComponentTester.hpp>
#include <Test/TestUtils.hpp>
// Oven
#include <Oven/Component.hpp>
#include <Oven/Entity.hpp>
#include <Oven/Managers/ComponentManager.hpp>
#include <Oven/Managers/TypeManager.hpp>
#include <Oven/Utils/Bag.hpp>
#include <Oven/Utils/Debug.hpp>

class ComponentA : public Component { ~ComponentA() {}};
class ComponentB : public Component { ~ComponentB() {}};
class ComponentC : public Component { ~ComponentC() {}};
class ComponentD : public Component { ~ComponentD() {}};
class ComponentE : public Component { ~ComponentE() {}};

static const int kMaxComp = 5;
static const int kEntityTot = 2;

void AddComp(ComponentManager& compMangager, Entity& e);
bool TestCompNum(ComponentManager& compMangager, Entity& e);
bool TestFillBag(ComponentManager& compManager, TypeManager &typeManager,
                 Entity& a, Entity& b);
bool CompTypeEqual(TypeManager& typeManager, Component* a, Component* b);
bool TestGetByType(ComponentManager& compManager, TypeManager &typeManager);
bool TestGetComponent(ComponentManager& compManager, TypeManager& typeManager,
                      Entity& e);
bool TestRemoveComponent(ComponentManager& compManager, Entity& e);
bool TestRemoveEntity(ComponentManager& compManager, Entity& e);

//----------------------------------------------------------------------------
//
void TestComponents() {
   printTestHeader("Test ComponentManager");
   int id = 0;
   bool result;
   Engine engine;
   Entity entityA(engine, id++);
   Entity entityB(engine, id++);
   ComponentManager& compManager = engine.getComponentManager();
   TypeManager& typeManager = compManager.getComponentTypeManager();

   D_CMD(printTestHeader("Testing Adding Components"));
   AddComp(compManager, entityA);
   result = TestCompNum(compManager, entityA);
   printResult(result, "Testing Adding Components");

   D_CMD(printTestHeader("Testing Fill Bag"));
   AddComp(compManager, entityB);
   result = TestFillBag(compManager, typeManager, entityA, entityB);
   printResult(result, "Testing Fill Bag");

   D_CMD(printTestHeader("Testing Get Type Bag"));
   result = TestGetByType(compManager, typeManager);
   printResult(result, "Testing Get Type Bag");

   D_CMD(printTestHeader("Testing Get Component"));
   result = TestGetComponent(compManager, typeManager, entityA);
   printResult(result, "Testing Get Component");

   D_CMD(printTestHeader("Testing Remove Component"));
   result = TestRemoveComponent(compManager, entityA);
   printResult(result, "Testing Remove Component");

   D_CMD(printTestHeader("Testing Remove Entity"));
   result = TestRemoveEntity(compManager, entityA);
   printResult(result, "Testing Remove Entity");

   // Must clear up component types
   Type::reset();
}

//----------------------------------------------------------------------------
//
void AddComp(ComponentManager& compMangager, Entity &e) {
   Component* curComp = new ComponentA();
   compMangager.addComponent(e, curComp);

   curComp = new ComponentB();
   compMangager.addComponent(e, curComp);

   curComp = new ComponentC();
   compMangager.addComponent(e, curComp);

   curComp = new ComponentD();
   compMangager.addComponent(e, curComp);

   curComp = new ComponentE();
   compMangager.addComponent(e, curComp);
}

//----------------------------------------------------------------------------
//
bool TestCompNum(ComponentManager& compMangager, Entity &e) {
   Bag<Component*> compBag;
   // Fills bag
   compMangager.fillComponentFor(e, compBag);

   return compBag.getCount() == kMaxComp;
}

//----------------------------------------------------------------------------
//
bool TestFillBag(ComponentManager& compManager, TypeManager& typeManager,
                 Entity& a, Entity& b) {
   bool result = true;
   Bag<Component*> compBagA;
   Bag<Component*> compBagB;

   compManager.fillComponentFor(a, compBagA);
   compManager.fillComponentFor(b, compBagB);

   for (int idx = 0; idx < kMaxComp; idx++) {
      result &= CompTypeEqual(typeManager, compBagA.get(idx), compBagB.get(idx));
   }

   return result;
}

//----------------------------------------------------------------------------
//
bool CompTypeEqual(TypeManager& typeManager, Component* a, Component* b) {
   const Type& typeA = typeManager.getTypeFor(a);
   const Type& typeB = typeManager.getTypeFor(a);

   return typeA.getId() == typeB.getId();
}

//----------------------------------------------------------------------------
//
bool TestGetByType(ComponentManager& compManager, TypeManager& typeManager) {
   Type& typeA =
         typeManager.getTypeFor<ComponentA>();
   Bag<Component*>* compBag = compManager.getComponentByType(typeA);
   D_COUT2("compBag total = " << compBag->getCount() << " total kEntityTot = "
           << kEntityTot << std::endl);

   return compBag->getCount() == kEntityTot;
}

//----------------------------------------------------------------------------
//
bool TestGetComponent(ComponentManager& compManager, TypeManager& typeManager,
                      Entity& e) {
   Component* comp = compManager.getComponent<ComponentA>(e);
   bool result = comp != NULL;

   if (result) {
      result &= typeManager.getId(comp)
            == typeManager.getId<ComponentA>();
   }

   return result;
}

//----------------------------------------------------------------------------
//
bool TestRemoveComponent(ComponentManager& compManager, Entity& e) {
   Bag<Component*> compBag;
   compManager.removeComponent<ComponentA>(e);
   compManager.fillComponentFor(e, compBag);

   return compBag.getCount() == kMaxComp - 1;
}

//----------------------------------------------------------------------------
//
bool TestRemoveEntity(ComponentManager& compManager, Entity& e) {
   Bag<Component*> compBag;

   compManager.deleted(&e);
   compManager.clean();
   compManager.fillComponentFor(e, compBag);

   return compBag.getCount() == 0;
}
