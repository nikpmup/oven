// Test
#include <Test/EntityTester.hpp>
#include <Test/TestUtils.hpp>

// Oven
#include <Oven/Entity.hpp>
#include <Oven/Managers/EntityManager.hpp>
#include <Oven/Utils/Debug.hpp>

static const int kFirstEntityId = 0;

// EntityManager
bool TestManagerAddEntity(EntityManager& entityManager);
bool TestManagerIsActive(EntityManager& entityManager);
bool TestManagerIsEnabled(EntityManager& entityManager);
bool TestManagerDisabling(EntityManager& entityManager);
bool TestManagerEnabling(EntityManager& entityManager);
bool TestManagerDeleting(EntityManager& entityManager);
// Entity
bool TestAddEntity(Entity* entity, EntityManager& em);

//----------------------------------------------------------------------------
//
void TestEntityManager() {
   printTestHeader("Test EntityManager");
   Engine engine;
   EntityManager entityManager;
   entityManager.setEngine(&engine);
   bool result;

   D_CMD(printTestHeader("Testing Adding Entity"));
   result = TestManagerAddEntity(entityManager);
   printResult(result, "Testing Adding Entity");

   D_CMD(printTestHeader("Testing Active Entity"));
   result = TestManagerIsActive(entityManager);
   printResult(result, "Testing Active Entity");

   D_CMD(printTestHeader("Testing Enabled Entity"));
   result = TestManagerIsEnabled(entityManager);
   printResult(result, "Testing Enabled Entity");

   D_CMD(printTestHeader("Testing Disabling Entity"));
   result = TestManagerDisabling(entityManager);
   printResult(result, "Testing Disabling Entity");

   D_CMD(printTestHeader("Testing Enabling Entity"));
   result = TestManagerEnabling(entityManager);
   printResult(result, "Testing Enabling Entity");
}

//----------------------------------------------------------------------------
//
bool TestManagerAddEntity(EntityManager &entityManager) {
   const int kCount = 1;
   bool result = true;

   // Creating Entity
   Entity* entity = entityManager.createEntityInstance();
   result &= entity->getId() == kFirstEntityId;
   D_COUT2("Result Entity Id : Passed = "
           << result << " id = " << entity->getId() << std::endl);
   result &= entityManager.getCreatedCount() == kCount;
   D_COUT2("Result Entity Count : Passed = " << result <<
           " count = " << entityManager.getCreatedCount() << std::endl);
   // Adding Entity
   entityManager.added(entity);
   result &= entityManager.getAddedCount() == kCount;
   result &= entityManager.getActiveEntityCount() == kCount;

   return result;
}

//----------------------------------------------------------------------------
//
bool TestManagerIsActive(EntityManager& entityManager) {
   return entityManager.isActive(kFirstEntityId);
}

//----------------------------------------------------------------------------
//
bool TestManagerIsEnabled(EntityManager &entityManager) {
   return entityManager.isEnabled(kFirstEntityId);
}

//----------------------------------------------------------------------------
//
bool TestManagerDisabling(EntityManager& entityManager) {
   bool result = true;
   // Getting Entity
   Entity* entity = entityManager.getEntity(kFirstEntityId);
   result &= entity->getId() == kFirstEntityId;
   // Disabling Entity
   entityManager.disabled(entity);
   result &= !entityManager.isEnabled(kFirstEntityId);

   return result;
}

//----------------------------------------------------------------------------
//
bool TestManagerEnabling(EntityManager& entityManager) {
   bool result = true;

   // Getting Entity
   Entity* entity = entityManager.getEntity(kFirstEntityId);
   result &= entity->getId() == kFirstEntityId;
   // Disabling Entity
   entityManager.enabled(entity);
   result &= entityManager.isEnabled(kFirstEntityId);

   return result;
}

//----------------------------------------------------------------------------
//
void TestEntity() {
   Engine engine;
   bool result;
   EntityManager& entityManager = engine.getEntityManager();
   Entity* entity = entityManager.createEntityInstance();

   printTestHeader("Test Entity");

   D_CMD(printTestHeader("Testing Adding Entity"));
   result = TestAddEntity(entity, entityManager);
   printResult(result, "Testing Adding Entity");
}

//----------------------------------------------------------------------------
//
bool TestAddEntity(Entity* entity, EntityManager& em) {
   entity->addToEngine();

   return em.getAddedCount() == 1;
}
