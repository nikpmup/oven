// Test
#include <Test/ComponentTypeTester.hpp>
#include <Test/TestUtils.hpp>

// Oven
#include <Oven/Component.hpp>
#include <Oven/ComponentType.hpp>
#include <Oven/Managers/TypeManager.hpp>
#include <Oven/Utils/Debug.hpp>

struct ComponentA : public Component {};
struct ComponentB : public Component {};
struct ComponentC : public Component {};
struct ComponentD : public Component {};
struct ComponentE : public Component {};

static const unsigned int kComponentCount = 5;

bool TypeIdTest(TypeManager& componentTypeManager);
bool TypeBitTest(TypeManager& componentTypeManager);
bool ResetTest(TypeManager& componentTypeManager);

//----------------------------------------------------------------------------
//
void TestComponentType() {
   TypeManager componentTypeManager;
   printTestHeader("Test ComponentTypes");
   D_COUT("Running TestComponentTypes\n");
   bool result;

   D_CMD(printTestHeader("Testing Unique Ids"));
   result = TypeIdTest(componentTypeManager);
   printResult(result, "Testing Unique Ids");

   D_CMD(printTestHeader("Testing Unique Bits"));
   result = TypeBitTest(componentTypeManager);
   printResult(result, "Testing Unique Bits");

   D_CMD(printTestHeader("Testing Reset"));
   result = ResetTest(componentTypeManager);
   printResult(result, "Testing Reset");
   Type::reset();
}

//----------------------------------------------------------------------------
//
bool TypeIdTest(TypeManager& componentTypeManager) {
   D_COUT("Running TypeIdTest\n");
   bool result = true;
   unsigned int typeIds[kComponentCount];
   unsigned int idx = 0;

   typeIds[idx++] = componentTypeManager.getId<ComponentA>();
   typeIds[idx++] = componentTypeManager.getId<ComponentB>();
   typeIds[idx++] = componentTypeManager.getId<ComponentC>();
   typeIds[idx++] = componentTypeManager.getId<ComponentD>();
   typeIds[idx] = componentTypeManager.getId<ComponentE>();

   for (idx = 0; idx < kComponentCount - 1; idx++) {
      for (unsigned int jdx = idx + 1; jdx < kComponentCount; jdx++) {
         result &= typeIds[jdx] != typeIds[idx];
         D_COUT2("Idx : " << idx << " and " << jdx
                 << " result " << result << std::endl);
      }
   }

   return result;
}

//----------------------------------------------------------------------------
//
bool TypeBitTest(TypeManager& componentTypeManager) {
   D_COUT("Running TypeBitTest\n");
   bool result = true;
   std::bitset<BIT_SIZE> bitSets[kComponentCount];
   unsigned int idx = 0;

   bitSets[idx++] = componentTypeManager.getBits<ComponentA>();
   bitSets[idx++] = componentTypeManager.getBits<ComponentB>();
   bitSets[idx++] = componentTypeManager.getBits<ComponentC>();
   bitSets[idx++] = componentTypeManager.getBits<ComponentD>();
   bitSets[idx] = componentTypeManager.getBits<ComponentE>();

   for (idx = 0; idx < kComponentCount - 1; idx++) {
      for (unsigned int jdx = idx + 1; jdx < kComponentCount; jdx++) {
         result &= bitSets[jdx] != bitSets[idx];
         D_COUT2("Idx : " << idx << " and " << jdx
                 << " result " << result << std::endl);
         D_COUT3("Idx Value : " << bitSets[idx] << " and Jdx Value : " << bitSets[jdx] << std::endl);
      }
   }

   return result;
}

//----------------------------------------------------------------------------
//
bool ResetTest(TypeManager& componentTypeManager) {
   const int max = kComponentCount - 1;
   D_COUT("Running TypeBitTest\n");
   bool result = true;
   std::bitset<BIT_SIZE> bitSets[max];
   unsigned int typeIds[max];
   int idx = 0;

   bitSets[idx] = componentTypeManager.getBits<ComponentA>();
   typeIds[idx++] = componentTypeManager.getId<ComponentA>();
   bitSets[idx] = componentTypeManager.getBits<ComponentB>();
   typeIds[idx++] = componentTypeManager.getId<ComponentB>();
   componentTypeManager.deleteTypes();
   Type::reset();
   bitSets[idx] = componentTypeManager.getBits<ComponentC>();
   typeIds[idx++] = componentTypeManager.getId<ComponentC>();
   bitSets[idx] = componentTypeManager.getBits<ComponentD>();
   typeIds[idx] = componentTypeManager.getId<ComponentD>();

   for (idx = 0; idx < max / 2; idx++) {
         result &= bitSets[idx] == bitSets[idx + 2];
         result &= typeIds[idx] == typeIds[idx + 2];
   }

   return result;
}
