// Oven
#include <Oven/Engine.hpp>
#include <Oven/ComponentType.hpp>
#include <Oven/Entity.hpp>
#include <Oven/Managers/ComponentManager.hpp>
#include <Oven/Managers/EntityManager.hpp>

//----------------------------------------------------------------------------
//
Engine::Engine()
   : mEntityManager(new EntityManager()),
     mComponentManager(new ComponentManager()) {
}

//----------------------------------------------------------------------------
//
Engine::~Engine() {
   delete mEntityManager;
   delete mComponentManager;
}

//----------------------------------------------------------------------------
//
EntityManager& Engine::getEntityManager() {
   return *mEntityManager;
}

//----------------------------------------------------------------------------
//
ComponentManager& Engine::getComponentManager() {
   return *mComponentManager;
}

//----------------------------------------------------------------------------
//
void Engine::addEntity(Entity *entity) {
   mAdded.add(entity);
}

//----------------------------------------------------------------------------
//
void Engine::changedEntity(Entity *entity) {
   mChanged.add(entity);
}

//----------------------------------------------------------------------------
//
void Engine::deletedEntity(Entity *entity) {
   if (!mDeleted.contains(entity))
      mDeleted.add(entity);
}

//----------------------------------------------------------------------------
//
void Engine::enableEntity(Entity *entity) {
   mEnabled.add(entity);
}

//----------------------------------------------------------------------------
//
void Engine::disableEntity(Entity *entity) {
   mDisabled.add(entity);
}
