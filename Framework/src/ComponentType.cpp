#include <Oven/ComponentType.hpp>

std::bitset<BIT_SIZE> ComponentType::sNextBit(1);
unsigned int ComponentType::sNextId(1);

//----------------------------------------------------------------------------
//
ComponentType::ComponentType()
   : mBit(sNextBit),
     mId(sNextId++) {
   sNextBit <<= 1;
}

//----------------------------------------------------------------------------
//
std::bitset<BIT_SIZE> ComponentType::getBits() const {
   return mBit;
}

//----------------------------------------------------------------------------
//
unsigned int ComponentType::getId() const {
   return mId;
}

//----------------------------------------------------------------------------
//
void ComponentType::reset() {
   sNextBit = 1;
   sNextId = 1;
}
