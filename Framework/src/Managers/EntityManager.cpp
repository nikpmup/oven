#include <Oven/Managers/EntityManager.hpp>
#include <Oven/Entity.hpp>

//----------------------------------------------------------------------------
//
IdentifierPool::IdentifierPool()
   : mNextAvailableId(0) {
}

//----------------------------------------------------------------------------
//
unsigned int IdentifierPool::checkout() {
   const unsigned int idMax = mIds.getCount();
   const unsigned int returnId = idMax > 0 ? mIds.removeLast() : mNextAvailableId++;

   return returnId;
}

//----------------------------------------------------------------------------
//
void IdentifierPool::checkIn(const unsigned int id) {
   mIds.add(id);
}

//----------------------------------------------------------------------------
//
EntityManager::EntityManager()
   : mActive(0),
     mAdded(0),
     mCreated(0),
     mDeleted(0) {
}

//----------------------------------------------------------------------------
//
EntityManager::~EntityManager() {
   const unsigned int kBagSize = mEntityBag.getCapacity();
   Entity* curEntity;

   for (unsigned int idx = 0; idx < kBagSize; idx++) {
      curEntity = mEntityBag.get(idx);

      if (curEntity != NULL) {
         delete curEntity;
      }
   }
}

//----------------------------------------------------------------------------
//
void EntityManager::initialize() {}

//----------------------------------------------------------------------------
//
Entity* EntityManager::createEntityInstance() {
   Entity* e = new Entity(*mEngine, mIdentifierPool.checkout());

   mCreated++;

   return e;
}

//----------------------------------------------------------------------------
//
void EntityManager::added(Entity* e) {
   mActive++;
   mAdded++;

   mEntityBag.set(e->getId(), e);
}

//----------------------------------------------------------------------------
//
void EntityManager::changed(Entity *e) {
}

//----------------------------------------------------------------------------
//
void EntityManager::deleted(Entity *e) {
   const int entityId = e->getId();

   // Removing from entity bag
   mEntityBag.set(entityId, NULL);

   // Removing from disabled
   mDisabled.reset(entityId);

   // Adding to id pool
   mIdentifierPool.checkIn(entityId);

   mActive--;
   mDeleted++;
}

//----------------------------------------------------------------------------
//
void EntityManager::enabled(Entity* e) {
   mDisabled.reset(e->getId());
}

//----------------------------------------------------------------------------
//
void EntityManager::disabled(Entity *e) {
   mDisabled.set(e->getId());
}

//----------------------------------------------------------------------------
//
bool EntityManager::isActive(const unsigned int entityId) {
   return mEntityBag.get(entityId) != NULL;
}

//----------------------------------------------------------------------------
//
bool EntityManager::isEnabled(const unsigned int entityId) {
   return !mDisabled[entityId];
}

//----------------------------------------------------------------------------
//
unsigned int EntityManager::getActiveEntityCount() {
   return mActive;
}

//----------------------------------------------------------------------------
//
unsigned int EntityManager::getCreatedCount() {
   return mCreated;
}

//----------------------------------------------------------------------------
//
unsigned int EntityManager::getAddedCount() {
   return mAdded;
}

//----------------------------------------------------------------------------
//
unsigned int EntityManager::getDeletedCount() {
   return mDeleted;
}

//----------------------------------------------------------------------------
//
Entity* EntityManager::getEntity(const unsigned int entity) {
   return mEntityBag.get(entity);
}
