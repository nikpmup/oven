#include <Oven/Managers/ComponentManager.hpp>

#include <Oven/Entity.hpp>
#include <Oven/Type.hpp>
#include <Oven/Utils/Debug.hpp>
#include <iostream>

//----------------------------------------------------------------------------
//
ComponentManager::ComponentManager() {
}

//----------------------------------------------------------------------------
//
ComponentManager::~ComponentManager() {
   const unsigned int typeMax = mComponentsByType.getCapacity();

   for (unsigned int idx = 0; idx < typeMax; idx++) {
      Bag<Component*>* compBag = mComponentsByType.get(idx);

      if (compBag != NULL) {
         const unsigned int compMax = compBag->getCapacity();

         for (unsigned int compIdx = 0; compIdx < compMax; compIdx++) {
            Component* comp = compBag->get(compIdx);

            if (comp != NULL) {
               delete comp;
            }
         }

         delete compBag;
      }
   }
}

//----------------------------------------------------------------------------
//
void ComponentManager::initialize() {
}

//----------------------------------------------------------------------------
//
void ComponentManager::deleted(Entity *e) {
   mDeleted.add(e);
}

//----------------------------------------------------------------------------
//
void ComponentManager::addComponent(Entity& entity,
                                    Component* comp) {
   const Type& compType =
         mComponentTypeManager.getTypeFor<Component>(comp);
   const unsigned int typeId = compType.getId();
   D_COUT2("Adding type id = " << typeId << std::endl);
   mComponentsByType.ensureCapacity(typeId);

   Bag<Component*>* compBag = mComponentsByType.get(typeId);
   if (compBag == NULL) {
      compBag = new Bag<Component*>();
      mComponentsByType.set(typeId, compBag);
   }

   compBag->set(entity.getId(), comp);
   entity.setComponentBit(typeId);
}

//----------------------------------------------------------------------------
//
void ComponentManager::removeComponent(Entity &entity, Type &type) {
   const unsigned int typeId = type.getId();

   Bag<Component*>* compBag = mComponentsByType.get(typeId);
   if (compBag != NULL) {
      const int entityId = entity.getId();
      Component* comp = compBag->get(entityId);

      compBag->set(entityId, NULL);
      entity.removeComponentBit(typeId);

      delete comp;
   }
}

//----------------------------------------------------------------------------
//
Bag<Component*>* ComponentManager::getComponentByType(Type &type) {
   const unsigned int typeId = type.getId();
   Bag<Component*>* typeBag = mComponentsByType.get(typeId);

   if (typeBag == NULL) {
      typeBag = new Bag<Component*>();
      mComponentsByType.set(typeId, typeBag);
   }

   return typeBag;
}

//----------------------------------------------------------------------------
//
Component* ComponentManager::getComponent(Entity& entity, Type& type) {
   const unsigned int typeId = type.getId();
   Bag<Component*>* typeBag = mComponentsByType.get(typeId);
   Component* comp = NULL;

   if (typeBag != NULL) {
      comp = typeBag->get(entity.getId());
   }

   return comp;
}

//----------------------------------------------------------------------------
//
void ComponentManager::fillComponentFor(Entity& entity, Bag<Component*>& fillBag) {
   const std::bitset<BIT_SIZE> compBits = entity.getComponentBits();

   for (int idx = 0; idx < BIT_SIZE; idx++) {
      if (compBits[idx] == 1) {
         Bag<Component*>* typeBag = mComponentsByType.get(idx);

         if (typeBag != NULL) {
            Component* comp = typeBag->get(entity.getId());

            if (comp != NULL)
               fillBag.add(comp);
         }
      }
   }
}

//----------------------------------------------------------------------------
//
void ComponentManager::clean() {
   const unsigned int count = mDeleted.getCount();

   if (count > 0) {
      for (unsigned int idx = 0; idx < count; idx++) {
         Entity* entity = mDeleted.get(idx);

         if (entity != NULL) {
            removeComponentsOfEntity(*entity);
         }
      }

      mDeleted.clear();
   }
}

//----------------------------------------------------------------------------
//
TypeManager& ComponentManager::getComponentTypeManager() {
   return mComponentTypeManager;
}

//----------------------------------------------------------------------------
//
void ComponentManager::removeComponentsOfEntity(Entity &entity) {
   const unsigned int entityId = entity.getId();
   std::bitset<BIT_SIZE> compBits = entity.getComponentBits();

   for (int idx = 0; idx < BIT_SIZE; idx++) {
      if (compBits[idx] == 1) {
         Bag<Component*>* typeBag = mComponentsByType.get(idx);

         if (typeBag != NULL) {
            Component* comp = typeBag->get(entityId);

            if (comp != NULL) {
               typeBag->set(entityId, NULL);
               delete comp;
            }
         }
      }
   }
}
