#include <Oven/Managers/TypeManager.hpp>

//----------------------------------------------------------------------------
//
TypeManager::TypeManager() {
}

//----------------------------------------------------------------------------
//
TypeManager::~TypeManager() {
   deleteTypes();
}

//----------------------------------------------------------------------------
//
void TypeManager::deleteTypes() {
   for (auto it = mTypes.begin(); it != mTypes.end(); ++it) {
      delete it->second;
   }
   mTypes.clear();

}

//----------------------------------------------------------------------------
//
Type& TypeManager::getTypeFor(const std::type_info& typeInfo) {
   Type* type = mTypes[&typeInfo];

   if (type == NULL) {
      type = new Type();
      mTypes[&typeInfo] = type;
   }

   return *type;
}

//----------------------------------------------------------------------------
//
unsigned int TypeManager::getId(const std::type_info& type) {
   return getTypeFor(type).getId();
}
