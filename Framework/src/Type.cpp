#include <Oven/Type.hpp>

std::bitset<BIT_SIZE> Type::sNextBit(1);
unsigned int Type::sNextId(1);

//----------------------------------------------------------------------------
//
Type::Type()
   : mBit(sNextBit),
     mId(sNextId++) {
   sNextBit <<= 1;
}

//----------------------------------------------------------------------------
//
std::bitset<BIT_SIZE> Type::getBits() const {
   return mBit;
}

//----------------------------------------------------------------------------
//
unsigned int Type::getId() const {
   return mId;
}

//----------------------------------------------------------------------------
//
void Type::reset() {
   sNextBit = 1;
   sNextId = 1;
}
