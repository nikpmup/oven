// Oven
#include <Oven/Entity.hpp>
#include <Oven/Engine.hpp>
#include <Oven/Managers/ComponentManager.hpp>
#include <Oven/Managers/EntityManager.hpp>


unsigned long Entity::sUniqCnt(0);

//----------------------------------------------------------------------------
//
Entity::Entity(Engine& engine, const unsigned int id)
   : mEngine(engine),
     mComponentManager(engine.getComponentManager()),
     mEntityManager(engine.getEntityManager()),
     mComponentTypeManager(mComponentManager.getComponentTypeManager()),
     mId(id),
     mUniqueId(sUniqCnt++) {
}

//----------------------------------------------------------------------------
//
unsigned int Entity::getId() {
   return mId;
}

//----------------------------------------------------------------------------
//
unsigned long Entity::getUniqueId() {
   return mUniqueId;
}

//----------------------------------------------------------------------------
//
Engine& Entity::getEngine() {
   return mEngine;
}

//----------------------------------------------------------------------------
//
std::bitset<BIT_SIZE> Entity::getComponentBits() {
   return mComponentBits;
}

//----------------------------------------------------------------------------
//
std::bitset<BIT_SIZE> Entity::getSystemBits() {
   return mSystemBits;
}

//----------------------------------------------------------------------------
//
void Entity::setComponentBit(const unsigned int typeId) {
   mComponentBits.set(typeId);
}

//----------------------------------------------------------------------------
//
void Entity::setSystemBit(const unsigned int typeId) {
   mSystemBits.set(typeId);
}

//----------------------------------------------------------------------------
//
void Entity::removeComponentBit(const unsigned int typeId) {
   mComponentBits.reset(typeId);
}

//----------------------------------------------------------------------------
//
void Entity::removeSystemBit(const unsigned int typeId) {
   mSystemBits.reset(typeId);
}

//----------------------------------------------------------------------------
//
void Entity::addComponent(Component* component) {
   mComponentManager.addComponent(*this, component);
}

//----------------------------------------------------------------------------
//
void Entity::removeComponent(Type &compType) {
   mComponentManager.removeComponent(*this, compType);
}

//----------------------------------------------------------------------------
//
Component* Entity::getComponent(Type &compType) {
   return mComponentManager.getComponent(*this, compType);
}

//----------------------------------------------------------------------------
//
void Entity::getComponents(Bag<Component*>& fillBag) {
   mComponentManager.fillComponentFor(*this, fillBag);
}

//----------------------------------------------------------------------------
//
bool Entity::isActive() {
   return mEntityManager.isActive(mId);
}

//----------------------------------------------------------------------------
//
bool Entity::isEnabled() {
   return mEntityManager.isEnabled(mId);
}

//----------------------------------------------------------------------------
//
void Entity::addToEngine() {
   mEngine.addEntity(this);
}

//----------------------------------------------------------------------------
//
void Entity::changedInEngine() {
   mEngine.changedEntity(this);
}

//----------------------------------------------------------------------------
//
void Entity::deletedFromEngine() {
   mEngine.deletedEntity(this);
}

//----------------------------------------------------------------------------
//
void Entity::enable() {
   mEngine.enableEntity(this);
}

//----------------------------------------------------------------------------
//
void Entity::disable() {
   mEngine.disableEntity(this);
}

//----------------------------------------------------------------------------
//
void Entity::reset() {
   mSystemBits.reset();
   mComponentBits.reset();
   mUniqueId = sUniqCnt++;
}
