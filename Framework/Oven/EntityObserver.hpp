#ifndef ENTITYOBSERVER_HPP
#define ENTITYOBSERVER_HPP

class Entity;
class EntityObserver {
public:
   virtual void added(Entity* e) = 0;
   virtual void changed(Entity* e) = 0;
   virtual void deleted(Entity* e) = 0;
   virtual void enabled(Entity* e) = 0;
   virtual void disabled(Entity* e) = 0;
};

#endif // ENTITYOBSERVER_HPP
