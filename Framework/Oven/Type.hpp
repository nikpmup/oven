#ifndef TYPE_HPP
#define TYPE_HPP

// STL
#include <bitset>

// Oven
#include <Oven/Utils/BitSize.hpp>

class Type {
public:
   Type();

   std::bitset<BIT_SIZE> getBits() const;
   unsigned int getId() const;

   static void reset();
private:
   const std::bitset<BIT_SIZE> mBit;
   const unsigned int mId;

   static std::bitset<BIT_SIZE> sNextBit;
   static unsigned int sNextId;
};

#endif // TYPE_HPP
