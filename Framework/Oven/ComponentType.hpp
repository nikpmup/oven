#ifndef COMPONENTTYPE_HPP
#define COMPONENTTYPE_HPP

// STL
#include <bitset>

// Oven
#include <Oven/Utils/BitSize.hpp>

class ComponentType {
public:
   ComponentType();

   std::bitset<BIT_SIZE> getBits() const;
   unsigned int getId() const;

   static void reset();
private:
   const std::bitset<BIT_SIZE> mBit;
   const unsigned int mId;

   static std::bitset<BIT_SIZE> sNextBit;
   static unsigned int sNextId;
};

#endif // COMPONENTTYPE_HPP
