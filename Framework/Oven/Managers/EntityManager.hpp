#ifndef ENTITYMANAGER_HPP
#define ENTITYMANAGER_HPP

// STL
#include <bitset>

// Oven
#include <Oven/EntityObserver.hpp>
#include <Oven/Manager.hpp>
#include <Oven/Utils/Bag.hpp>
#include <Oven/Utils/BitSize.hpp>

class IdentifierPool {
public:
   IdentifierPool();

   unsigned int checkout();
   void checkIn(const unsigned int id);

private:
   Bag<unsigned int> mIds;
   unsigned int mNextAvailableId;
};

class Entity;
class EntityManager : public EntityObserver, public Manager {
public:
   // Constructor
   EntityManager();
   ~EntityManager();

   // Initialize
   void initialize();

   // Entity Managing
   void added(Entity* e);
   void changed(Entity *e); // Not used
   void deleted(Entity* e);
   void enabled(Entity* e);
   void disabled(Entity* e);
   Entity* createEntityInstance();
   Entity* getEntity(const unsigned int entityId);

   // Validate
   bool isActive(const unsigned int entityId);
   bool isEnabled(const unsigned int entityId);

   // Info
   unsigned int getActiveEntityCount();
   unsigned int getCreatedCount();
   unsigned int getAddedCount();
   unsigned int getDeletedCount();


private:
   Bag<Entity*> mEntityBag;
   std::bitset<BIT_SIZE> mDisabled;

   unsigned int mActive;
   unsigned int mAdded;
   unsigned int mCreated;
   unsigned int mDeleted;

   IdentifierPool mIdentifierPool;
};

#endif // ENTITYMANAGER_HPP
