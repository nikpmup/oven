#ifndef COMPONENTMANAGER_HPP
#define COMPONENTMANAGER_HPP

// Oven
#include <Oven/Component.hpp>
#include <Oven/Manager.hpp>
#include <Oven/Managers/TypeManager.hpp>
#include <Oven/Utils/Bag.hpp>

class Entity;
class Type;
class ComponentManager : public Manager {
public:
   ComponentManager();
   ~ComponentManager();
   virtual void initialize();
   void deleted(Entity* e);
   void fillComponentFor(Entity& entity, Bag<Component*>& fillBag);
   void addComponent(Entity& entity, Component* comp);

   // Removing Components
   void removeComponent(Entity& entity, Type& type);

   template <typename T>
   inline void removeComponent(Entity& entity) {
      removeComponent(entity, mComponentTypeManager.getTypeFor<T>());
   }

   // Getting Components
   Bag<Component*>* getComponentByType(Type& type);
   Component* getComponent(Entity& entity, Type& type);

   template <typename T>
   inline Component* getComponent(Entity& entity) {
      return getComponent(entity,
                          mComponentTypeManager.getTypeFor<T>());
   }

   void clean();

   TypeManager& getComponentTypeManager();

private:
   TypeManager mComponentTypeManager;
   Bag<Bag<Component*>*> mComponentsByType;
   Bag<Entity*> mDeleted;

   void removeComponentsOfEntity(Entity &entity);
};

#endif // COMPONENTMANAGER_HPP
