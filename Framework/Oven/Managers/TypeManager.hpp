#ifndef TYPEMANAGER_HPP
#define TYPEMANAGER_HPP

// STL
#include <bitset>
#include <map>
#include <typeinfo>

// Oven
#include <Oven/Type.hpp>
#include <Oven/Utils/BitSize.hpp>

class TypeManager {
public:
   // Constructor
   TypeManager();

   // Deconstrucotrs
   ~TypeManager();

   // Clean
   void deleteTypes();

   // Getting Type
   Type& getTypeFor(const std::type_info& typeInfo);

   template <typename T>
   Type& getTypeFor(T* obj) {
      const std::type_info& typeInfo = typeid(*obj);

      return getTypeFor(typeInfo);
   }

   template <typename K>
   Type& getTypeFor() {
      return getTypeFor(typeid(K));
   }

   template <typename K>
   std::bitset<BIT_SIZE> getBits() {
      return getTypeFor<K>().getBits();
   }

   // Getting Id
   unsigned int getId(const std::type_info& type);

   template <typename T>
   unsigned int getId(T* obj) {
      return getTypeFor(obj).getId();
   }

   template <typename K>
   unsigned int getId() {
      return getTypeFor<K>().getId();
   }

private:
   struct type_info_comparator {
      bool operator () (const std::type_info* a, const std::type_info* b) {
         return a->before(*b);
      }
   };

   typedef std::map<const std::type_info*, Type*,
         type_info_comparator> TypeMap;

   TypeMap mTypes;
};

#endif // TYPEMANAGER_HPP
