#ifndef ENGINE_HPP
#define ENGINE_HPP

// Oven
#include <Oven/Utils/Bag.hpp>
// STL
#include <typeinfo>

class Entity;
class EntityManager;
class ComponentManager;
class Manager;
class Engine {
public:
   // Constructors
   Engine();
   ~Engine();

   // Manager Getters
   EntityManager& getEntityManager();
   ComponentManager& getComponentManager();

   // Entity Managing
   void addEntity(Entity* entity);
   void changedEntity(Entity* entity);
   void deletedEntity(Entity* entity);
   void enableEntity(Entity* entity);
   void disableEntity(Entity* entity);

private:
   EntityManager* mEntityManager;
   ComponentManager* mComponentManager;
   Bag<Entity*> mAdded;
   Bag<Entity*> mChanged;
   Bag<Entity*> mDeleted;
   Bag<Entity*> mEnabled;
   Bag<Entity*> mDisabled;
   Bag<Manager*> mManagers;
};

#endif // ENGINE_HPP
