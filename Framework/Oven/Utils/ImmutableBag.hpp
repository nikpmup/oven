#ifndef IMMUTABLEBAG_HPP
#define IMMUTABLEBAG_HPP

template<typename T>
class ImmutableBag {
public:
   virtual T get(unsigned int index) = 0;
   virtual unsigned int getCapacity() = 0;
   virtual unsigned int getCount() = 0;
   virtual bool isEmpty() = 0;
   virtual ~ImmutableBag() {}
};

#endif // IMMUTABLEBAG_HPP
