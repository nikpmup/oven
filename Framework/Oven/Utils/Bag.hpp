#ifndef BAG_HPP
#define BAG_HPP

// Std Libraries
#include <cstddef>

// Oven
#include <Oven/Utils/ImmutableBag.hpp>

template <typename T>
class Bag : public ImmutableBag<T> {
public:
   Bag()
      : mCount(0),
        mCapacity(32),
        mData(new T[32]) {
      clear();
   }

   Bag(const unsigned int capacity)
      : mCount(0),
        mCapacity(capacity),
        mData(new T[capacity]) {
      clear();
   }

   virtual ~Bag() {
      delete[] mData;
   }

   // Remove functions
   T removeIdx(const unsigned int idx) {
      T rmObj = mData[idx];
      mData[idx] = mData[--mCount];
      mData[mCount] = (T) NULL;

      return rmObj;
   }

   T removeLast() {
      T rmObj = (T) NULL;

      if (!isEmpty()) {
         rmObj = removeIdx(mCount - 1);
         set(--mCount, (T) NULL);
      }

      return rmObj;
   }

   bool remove(T obj) {
      for (unsigned int idx = 0; idx < mCount; idx++) {
         if (mData[idx] == obj) {
            removeIdx(idx);
            return true;
         }
      }

      return false;
   }

   bool removeAll(Bag<T> bag) {
      bool removed = false;

      for (unsigned int idx = 0; idx < bag.mCount; idx++) {
         if (remove(bag.mData[idx]))
            removed = true;
      }

      return removed;
   }

   bool contains(T obj) {
      bool retVal = false;
      for (unsigned int idx = 0; idx < mCapacity && !retVal; idx++) {
         retVal = obj == mData[idx];
      }
      return retVal;
   }

   bool isWithinBounds(const unsigned int idx) {
      return idx < mCapacity;
   }

   bool isEmpty() {
      return !mCount;
   }

   // Get
   T get(const unsigned int idx) {
      return idx < mCapacity ? mData[idx] : (T) NULL;
   }

   void clear() {
      for (unsigned int idx = 0; idx < mCapacity; idx++) {
         mData[idx] = (T) NULL;
      }

      mCount = 0;
   }

   // Data
   unsigned int getCount() {
      return mCount;
   }

   unsigned int getCapacity() {
      return mCapacity;
   }

   // Adding
   void add(T obj) {
      if (mCount >= mCapacity)
         grow();

      mData[mCount++] = obj;
   }

   void addAll(Bag<T> bag) {
      for (unsigned int idx = 0; idx < bag.mCapacity; idx++) {
         add(bag[idx]);
      }
   }

   void set(const unsigned int idx, T obj) {
      if (idx >= mCapacity) {
         grow(idx * 2);
      }

      if (obj == (T) NULL && mData[idx] != (T) NULL) {
         mCount--;
      } else if (obj != (T) NULL && mData[idx] == (T) NULL) {
         mCount++;
      }

      mData[idx] = obj;
   }

   void ensureCapacity(const unsigned int idx) {
      if (idx >= mCapacity)
         grow(idx * 2);
   }

private:
   unsigned int mCount;
   unsigned int mCapacity;
   T* mData;

   void grow() {
      unsigned int newCapacity = (mCapacity * 3.0f) * 0.5f + 1.0f;

      grow(newCapacity);
   }

   void grow(const unsigned int capacity) {
      T* newData = new T[capacity];

      for (unsigned int idx = 0; idx < mCapacity; idx++) {
         newData[idx] = mData[idx];
      }

      for (unsigned int idx = mCapacity; idx < capacity; idx++) {
         newData[idx] = (T) NULL;
      }

      delete[] mData;

      mCapacity = capacity;
      mData = newData;
   }

};

#endif // BAG_HPP
