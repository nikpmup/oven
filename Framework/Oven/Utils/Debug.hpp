#ifndef DEBUG_HPP
#define DEBUG_HPP

#define DEBUG 0

#include <iostream>

#if DEBUG > 0
   #define D_COUT(x) std::cout << "Debug L1: " << x
   #define D_CMD(x) x
#else
   #define D_COUT(x)
   #define D_CMD(x)
#endif

#if DEBUG > 1
   #define D_COUT2(x) std::cout << "Debug L2: " << x
#else
   #define D_COUT2(x)
#endif

#if DEBUG > 2
   #define D_COUT3(x) std::cout << "Debug L3: " << x
#else
   #define D_COUT3(x)
#endif

#endif // DEBUG_HPP
