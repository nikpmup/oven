#ifndef MANAGER_HPP
#define MANAGER_HPP

// STL
#include <memory>
#include <Oven/Engine.hpp>

class Manager {
public:
   void setEngine(Engine* engine) {
      mEngine = engine;
   }

   Engine& getEngine() {
      return *mEngine;
   }

   virtual ~Manager() {}

protected:
   Engine* mEngine;

   virtual void initialize() = 0;
};

#endif // MANAGER_HPP
