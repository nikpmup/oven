#ifndef ENTITY_HPP
#define ENTITY_HPP

// STL
#include <bitset>

// Oven
#include <Oven/ComponentType.hpp>
#include <Oven/Managers/TypeManager.hpp>
#include <Oven/Utils/BitSize.hpp>
#include <Oven/Utils/Bag.hpp>

class Engine;
class Component;
class EntityManager;
class ComponentManager;
class Entity {
public:
   Entity(Engine& engine, const unsigned int id);
   // Id
   unsigned int getId();
   unsigned long getUniqueId();
   Engine& getEngine();

   // Bitsets
   std::bitset<BIT_SIZE> getComponentBits();
   std::bitset<BIT_SIZE> getSystemBits();
   void setComponentBit(const unsigned int typeId);
   void setSystemBit(const unsigned int typeId);
   void removeComponentBit(const unsigned int typeId);
   void removeSystemBit(const unsigned int typeId);

   // Components
   void addComponent(Component* component);
   void removeComponent(Type& compType);

   template<typename T>
   void removeComponent() {
      removeComponent(mComponentTypeManager.getTypeFor<T>());
   }

   Component* getComponent(Type& compType);

   template<typename T>
   Component* getComponent() {
      return getComponent(mComponentTypeManager.getTypeFor<T>());
   }

   void getComponents(Bag<Component*>& fillBag);

   // State
   bool isActive();
   bool isEnabled();

   // Engine State
   void addToEngine();
   void changedInEngine();
   void deletedFromEngine();
   void enable();
   void disable();

   // Clearing
   void reset();

private:
   Engine& mEngine;
   ComponentManager& mComponentManager;
   EntityManager& mEntityManager;
   TypeManager& mComponentTypeManager;

   unsigned int mId;
   unsigned long mUniqueId;
   std::bitset<BIT_SIZE> mComponentBits;
   std::bitset<BIT_SIZE> mSystemBits;

   static unsigned long sUniqCnt;
};

#endif // ENTITY_HPP
