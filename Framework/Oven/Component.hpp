#ifndef COMPONENT_HPP
#define COMPONENT_HPP

class Component {
public:
   virtual ~Component() {}
protected:
   Component() {}
};

#endif // COMPONENT_HPP
