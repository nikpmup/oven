Engine
- Contains Managers
- Provides API to expose framework

Entity
- Provides API from id, component types, (systems?)
- If has API form adding components
   -- Requries reference to ComponentManager

ComponentManager
- Holds list of all components
   -- Organized by Bag(ComponentTypes) = returns Bag(Entity)*
                     -> Bag(Entity) = returns Component*
- Provides API to manipulate components using Entity
!- creates circular dependency of Entity and ComponentManager
